libcatalyst-modules-perl (49) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 09 Jun 2022 21:57:01 +0100

libcatalyst-modules-perl (48+nmu1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 04 Jan 2021 15:18:20 +0100

libcatalyst-modules-perl (48) unstable; urgency=medium

  * convert to metapackage depending on previously bundled modules, now
    separate packages
    + purge dependencies and the build system, update description, add NEWS
      entry
  * Add myself to Uploaders

 -- Damyan Ivanov <dmn@debian.org>  Mon, 19 May 2014 20:04:09 +0300

libcatalyst-modules-perl (47) unstable; urgency=medium

  * Update Catalyst-Controller-HTML-FormFu.
    Thanks to David Suárez for the bug report. (Closes: #733429)
  * Update year of upstream copyright for Catalyst-Controller-HTML-FormFu.
  * Update versioned (build) dependency on libhtml-formfu-perl.
  * Add build dependency on libtest-aggregate-perl.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Sun, 29 Dec 2013 16:40:31 +0100

libcatalyst-modules-perl (46) unstable; urgency=low

  * Add libcatalyst-dispatchtype-regex-perl to Build-Depends-Indep and
    Suggests. (Closes: #718082)
  * Update Catalyst-Plugin-Session-Store-DBIC from 0.13 to 0.14.

 -- gregor herrmann <gregoa@debian.org>  Mon, 29 Jul 2013 20:31:39 +0200

libcatalyst-modules-perl (45) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ gregor herrmann ]
  * Update modules:
    + Catalyst-Plugin-Cache                            0.12
    + Catalyst-Plugin-ConfigLoader                     0.32
    + Catalyst-Plugin-Session                          0.37
    + Catalyst-Plugin-StackTrace                       0.12
    + Catalyst-Plugin-SubRequest                       0.20
    + Catalyst-Model-DBI                               0.32
    + Catalyst-Model-DBIC-Schema                       0.61
    + Catalyst-Plugin-Session-Store-FastMmap           0.16
    + Catalyst-Plugin-Authentication                   0.10023
    + Catalyst-View-Email                              0.33
    + Catalyst-Authentication-Credential-Authen-Simple 0.09
    + Catalyst-Authentication-Store-DBIx-Class         0.1505
    + Catalyst-Controller-HTML-FormFu                  0.09004
    + Catalyst-Plugin-Session-Store-DBIC               0.13
  * Update upstream and packaging copyright.
  * Update build and runtime dependencies.
  * Disable pod{,-coverage} tests.
  * Drop patch Catalyst-Plugin-Session-State-Cookie/Disable-Broken-Live-
    Test, (build-)depend on libcatalyst-perl 5.90015.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Jun 2013 18:54:12 +0200

libcatalyst-modules-perl (44) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.
  * Drop (build-)dep on libemail-send-perl. Catalyst::View::Email now uses
    libemail-sender-perl and there don't seem to be other modules using it.
    (Closes: #639881)

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ intrigeri ]
  * Update README.source to match the SVN->Git migration.
  * Catalyst-Plugin-Session-State-Cookie:
    new patch: Disable-Broken-Live-Test (temporarily workaround for #665222).
  * Build-depend on perl (>= 5.14.2) | libtest-simple-perl (>= 0.94):
    this dependency is now satisfied by Perl.
  * Build-depend on libcatalyst-devel-perl,
    to run more of the Catalyst::Model::DBIC::Schema test suite.
  * Updated modules:
    + Catalyst-Plugin-Session 0.35
      - add new build-dep on libtest-www-mechanize-psgi-perl
    + Catalyst-Plugin-Cache 0.11
      - bump dependency on libcatalyst-perl to (>= 5.80025)
    + Catalyst-Plugin-Static-Simple 0.30
    + Catalyst-Plugin-SubRequest 0.18
      - bump dependency on Catalyst-Runtime
    + Catalyst-Log-Log4perl 1.06
    + Catalyst-Model-DBIC-Schema 0.60
      - add dependencies on libmoosex-markasmethods-perl (>= 0.13)
        and libmoosex-nonmoose-perl (>= 0.16)
    + Catalyst-Plugin-Authentication 0.10019
      - add new dependency on libmoosex-emulate-class-accessor-fast-perl
    + Catalyst-Authentication-Store-DBIx-Class 0.1503
    + Catalyst-Plugin-Session-Store-DBIC 0.12
  * Move Catalyst-Component-InstancePerContext to 02/:
    Catalyst-Model-DBIC-Schema now needs it.
  * debian/copyright:
    - separate files with space, not commas.
    - formally use new 1.0 copyright format specification.
  * Bump Standards-Version to 3.9.3, no change required.

 -- intrigeri <intrigeri@debian.org>  Tue, 26 Jun 2012 19:57:20 +0200

libcatalyst-modules-perl (43) unstable; urgency=low

  * Switch order of alternative (build) dependencies after the perl 5.12
    upload.
  * Updated modules:
    + Catalyst-Action-RenderView 0.16
    + Catalyst-Plugin-SubRequest 0.17
    + Catalyst-Model-DBIC-Schema 0.49
    + Catalyst-View-JSON 0.33
    + Catalyst-Plugin-Authentication 0.10017
    + Catalyst-Plugin-Authorization-Roles 0.09
    + Catalyst-View-Email 0.31
    + Catalyst-Authentication-Store-DBIx-Class 0.1500
    + Catalyst-Controller-FormBuilder 0.06
    + Catalyst-Controller-HTML-FormFu 0.09003 (closes: #627238)
  * Set Standards-Version to 3.9.2 (no changes).
  * Bump debhelper compatibility level to 8.
  * Add (build) dependency on parent; bump versioned (build) dependency on
    HTML::FormFu.
  * Update copyright information for some updated modules.

 -- gregor herrmann <gregoa@debian.org>  Fri, 20 May 2011 00:36:53 +0200

libcatalyst-modules-perl (42) unstable; urgency=low

  [ gregor herrmann ]
  * debian/rules: switch order of arguments to dh.
  * Updated modules:
    + Catalyst-Authentication-Store-DBIx-Class
    + Catalyst-Model-DBIC-Schema
    + Catalyst-Plugin-Cache
    + Catalyst-Plugin-ConfigLoader
    + Catalyst-Plugin-I18N
    + Catalyst-Plugin-Session
    + Catalyst-Plugin-Session-Store-FastMmap
    + Catalyst-View-Email
    + Catalyst-View-JSON
    + CatalystX-Component-Traits (closes: #615162)
  * Update debian/copyright.
  * Set Standards-Version to 3.9.1 (no further changes).
  * debian/control:
    - make (build) dependency on libconfig-any-perl versioned
    - add (build) dependency on libmoosex-nonmoose-perl
    - add libtest-use-ok-perl to Build-Depends-Indep (closes: #616004)
    - update (build) dependencies for Catalyst::Model::DBIC::Schema
    - add build dependency on libtest-requires-perl.

  [ Ansgar Burchardt ]
  * Update my email address.

  [ Jonathan Yu ]
  * Updated modules:
    + Catalyst::Plugin::Session::Store::FastMmap 0.14 (was 0.13)
    + Catalyst::Model::DBIC::Schema 0.48 (was 0.43)
    + Catalyst::Authentication::Store::DBIx::Class 0.1401 (was 0.1400)
  * Update ansgar's email address in packages.cfg

 -- gregor herrmann <gregoa@debian.org>  Fri, 11 Mar 2011 15:26:57 +0100

libcatalyst-modules-perl (41) unstable; urgency=low

  [ Jonathan Yu ]
  * Updated modules:
    + Catalyst::Model::DBIC::Schema 0.41 (was 0.40)
    + Catalyst::View::JSON 0.30 (was 0.28)
    + Catalyst::Plugin::Session::Store::Delegate 0.06 (was 0.05)
    + Catalyst::Authentication::Store::DBIx::Class 0.1200 (was 0.1083)

  [ gregor herrmann ]
  * debian/copyright: update years of upstream copyright for
    Catalyst::Model::DBIC::Schema.

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 09 May 2010 08:37:56 -0400

libcatalyst-modules-perl (40) unstable; urgency=low

  * Updated modules:
    + Catalyst::Plugin::Session::Store::DBI 0.16 (was 0.15)
    + Catalyst::View::Email 0.27 (was 0.23)
  * Added information about this bundle to README.source and wrapped
    the existing quilt writeup accordingly
  * Use new 3.0 (native) source format

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 04 Apr 2010 21:33:09 -0400

libcatalyst-modules-perl (39) unstable; urgency=low

  [ Jonathan Yu ]
  * Updated modules:
    + Catalyst::View::JSON 0.28 (was 0.26)
    + Catalyst::Authentication::Store::DBIx::Class 0.1083 (was 0.1082)
    + Catalyst::Plugin::Cache 0.09 (was 0.08)
  * Catalyst::Devel was moved into its own package
  * Update debian/* years of copyright
  * Moved debian/copyright information to new DEP5 format

  [ gregor herrmann ]
  * Updated:
    + Catalyst::Plugin::Unicode 0.93 (was 0.92)

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 18 Mar 2010 10:26:16 -0400

libcatalyst-modules-perl (38) unstable; urgency=low

  * Updated modules:
    + Catalyst::Plugin::Static::Simple 0.29 (was 0.28)
    + Catalyst::Model::DBIC::Schema 0.40 (was 0.38)
    + Catalyst::View::Email 0.23 (was 0.22)
  * Rewrite NEWS without asterisks

 -- Jonathan Yu <jawnsy@cpan.org>  Mon, 15 Feb 2010 14:46:26 -0500

libcatalyst-modules-perl (37) unstable; urgency=low

  * debian/make-module.sh: use dh_* helpers and unify indentation.
    Export PERL5LIB for the tests.
  * Set Standards-Version to 3.8.4 (no changes).
  * debian/rules: add back patch/unpatch targets that got lost in some
    previous update.
  * Remove patch for Catalyst-Plugin-Flavour, which was removed in 35.
  * Updates modules:
    + Catalyst::Plugin::SubRequest 0.16
    + Catalyst::Model::DBI 0.28
    + Catalyst::View::Email 0.22 (closes: #567478)
  * Update debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 29 Jan 2010 15:13:33 +0100

libcatalyst-modules-perl (36) unstable; urgency=low

  [ Jonathan Yu ]
  * Updated modules:
    + Catalyst::Action::RenderView 0.14
    + Catalyst::Authentication::Credential::Authen::Simple 0.07
    + Catalyst::Controller::HTML::FormFu 0.06001
    + Catalyst::Plugin::Authorization::Roles 0.08
    + Catalyst::Plugin::SubRequest 0.15 (closes: #566373)
  * Refresh copyright information

  [ gregor herrmann ]
  * Updated modules:
    + Catalyst::Devel 1.26
    + Catalyst::Model::DBI 0.25
    + Catalyst::Model::DBIC::Schema 0.38
    + Catalyst::Plugin::Authentication 0.10016
    + Catalyst::Plugin::Static::Simple 0.28
    + Catalyst::View::Email 0.19
  * Build depend on libtest-simple-perl >=0.94.
  * Add (build) dependency on libemail-sender-perl (needed by
    Catalyst::View::Email).
  * Update debian/copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 24 Jan 2010 00:33:00 +0100

libcatalyst-modules-perl (35) unstable; urgency=low

  [ Jonathan Yu ]
  * Add Catalyst-Plugin-Session-Store-Cache for MojoMojo
  * Moved Catalyst::Model::DBI and Catalyst::Model::DBIC::Schema from
    01 to 02 sequence order; needs the new CatalystX::Component::Traits
  * Moved the following modules from 04 to 05 sequence order:
    + Catalyst::Authentication::Store::DBIx::Class
    + Catalyst::Authentication::Credential::Authen::Simple
    + Catalyst::Controller::FormBuilder
    + Catalyst::Plugin::Authorization::ACL
    because they need Catalyst::Plugin::Authorization::Roles
  * Moved Catalyst::Plugin::Session::Store::DBIC from 03 to 05 sequence
    order since it needs Catalyst::Plugin::Authorization::Roles
  * Remove modules considered dead upstream:
    - Catalyst::Plugin::Prototype
    - Catalyst::Plugin::HTML::Widget
    - Catalyst::Plugin::Dumper
    - Catalyst::Plugin::XMLRPC
    - Catalyst::Plugin::DefaultEnd
  * Remove modules with unresponsive upstream + missing copyright:
    - Catalyst::Plugin::Flavour
  * Remove Catalyst::Plugin::Server -- this is less than ideal, but it
    doen't build at this time, and as long as it's part of this bundle,
    we cannot patch it without repacking. It should be placed in a NEW
    package.
  * Note many deprecated modules in Debian.NEWS
  * Updated modules:
    + Catalyst::Model::DBI 0.24
    + Catalyst::Model::DBIC::Schema 0.31
    + Catalyst::Plugin::ConfigLoader 0.27
    + Catalyst::Plugin::Session 0.27
    + Catalyst::Plugin::Static::Simple 0.25
    + Catalyst::Plugin::Unicode 0.92
    + Catalyst::Devel 1.21
    + Catalyst::Plugin::Session 0.29
    + Catalyst::Plugin::Session::State::Cookie 0.17
    + Catalyst::Plugin::Session::Store::Delegate 0.05
    + Catalyst::Plugin::Session::Store::FastMmap 0.13
    + Catalyst::Plugin::Session::Store::File 0.18
    + Catalyst::Plugin::Session::Store::DBI 0.15
    + Catalyst::Plugin::Session::Store::DBIC 0.11
    + Catalyst::Plugin::SubRequest 0.14
    + Catalyst::Plugin::Authentication 0.10015
    + Catalyst::View::JSON 0.26
    + Catalyst::Plugin::Authorization::ACL 0.15
    + Catalyst::Controller::HTML::FormFu 0.05000
    + Catalyst::Log::Log4perl 1.04
    + Catalyst::Plugin::StackTrace 0.11
    + Catalyst::Action::RenderView 0.13
  * Change to search.cpan.org/dist for watch entries
  * Add a Debian NEWS entry to note added/removed things
  * Remove libcatalyst-modules-perl.lintian-overrides as it is no longer
    needed (XMLRPC was removed; thus it has no manpages)
  * Add Test::Log4perl and Test::File to B-D-I for testing

  [ Ansgar Burchardt ]
  * debian/copyright: Add copyright information for
    Catalyst::Plugin::Session::Store::Cache
  * debian/control: Make dependency on perl unversioned.
  * Bump Standards-Version to 3.8.3.
  * Updated modules:
    + Catalyst::Action::RenderView 0.11
    + Catalyst::View::Mason 0.18:
      - works with Catalyst::Runtime 0.80010 (Closes: #547368)

  [ gregor herrmann ]
  * debian/control: change (build) dependency on "libemail-mime-
    {creator,modifier}-perl" to "libemail-mime-perl (>= 1.901) |
    libemail-mime-{creator,modifier}-perl".
  * debian/control: add a build dependency on "perl (>= 5.10.1) |
    libtest-simple-perl (>= 0.88)", CatalystX::Component::Traits needs
    Test::More >= 0.88.
  * Bump debhelper build dependency to 7.2.13 because of Module::AutoInstall
    usage.

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 05 Nov 2009 16:50:48 -0500

libcatalyst-modules-perl (34) unstable; urgency=low

  [ Krzysztof Krzyżaniak (eloy) ]
  * Updated modules:
    + Catalyst::Devel 1.15
    + Catalyst::Plugin::Session 0.22
    + Catalyst::Plugin::Session::State::Cookie 0.11
    + Catalyst::Plugin::Session::Store::Delegat 0.04
    + Catalyst::Plugin::Session::Store::FastMmap 0.10
    + Catalyst::Plugin::Session::Store::File 0.17

  [ gregor herrmann ]
  * Updated modules:
    + Catalyst::Action::RenderView 0.10
    + Catalyst::Authentication::Credential::Authen::Simple 0.05
    + Catalyst::Controller::HTML::FormFu 0.04003
    + Catalyst::Plugin::Authorization::ACL 0.11
    + Catalyst::Plugin::ConfigLoader 0.23
    + Catalyst::Plugin::DefaultEnd 0.07
    + Catalyst::Plugin::StackTrace 0.10
    + Catalyst::Plugin::Unicode 0.91 (closes: #527940)
  * debian/make-module.sh: add PERL5_CPANPLUS_IS_RUNNING=1 to keep
    Module::AutoInstall from loading CPAN.pm at build time.
  * debian/control: add build dependencies on libtest-warn-perl,
    libauthen-sasl-perl, and build/runtime dependency on
    libfile-changenotify-perl.
  * Move Catalyst-Plugin-Session-Store-DBIC-0.09.tar.gz from tarballs/02 to
    tarballs/03 since it needs
    tarballs/02/Catalyst-Plugin-Session-Store-Delegate-0.03.tar.gz.
  * Split lintian overrides for source into debian/source.lintian-overrides.
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Ansgar Burchardt ]
  * Catalyst-Plugin-Session needs libmoosex-emulate-class-accessor-fast-perl
    (>= 0.00801) for test suite.
  * debian/make-module.sh now fails on error.
  * Add quilt framework.
  * Disable t/pod-coverage in Catalyst-Plugin-Flavour.
    + New patch: Catalyst-Plugin-Flavour/disable-pod-coverage-test.patch
  * debian/rules: Depend on *-stamp targets in order to avoid building
    everything twice.
  * Updated modules:
    + Catalyst::Controller::FormBuilder 0.05
  * debian/control: Add Vcs-* fields.
  * Bump Standards-Version to 3.8.2 (no changes).
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 22 Jul 2009 04:41:10 +0200

libcatalyst-modules-perl (33) unstable; urgency=low

  [ Ryan Niebur ]
  * Remove Florian Ragwitz from Uploaders (Closes: #523244)

  [ gregor herrmann ]
  * Updated modules:
    + Catalyst::Authentication::Credential::Authen::Simple 0.03
    + Catalyst::Plugin::I18N 0.09
    + Catalyst::Plugin::Session::Store::DBIC 0.09
    + Catalyst::Plugin::Session::Store::FastMmap 0.07
    + Catalyst::Plugin::Session::Store::File 0.15
    + Catalyst::Plugin::Static::Simple 0.21
    + Catalyst::Plugin::Unicode 0.9
    + Catalyst::View::JSON 0.25
    + Catalyst::View::Mason 0.17
  * New modules:
    + Catalyst::Controller::HTML::FormFu 0.04001 (closes: #508273)
    + Catalyst::Component::InstancePerContext 0.001001 (closes: #508262)
  * debian/control:
    - add build and runtime dependencies for the new modules
    - add /me to Uploaders
  * Update list of included modules in debian/control and debian/copyright.
  * Add an additonal lintian override, remove the obsolete ones.
  * debian/rules:
    - fix permissions of a POD file
    - move /usr/bin/rpc_client to examples

 -- Ryan Niebur <ryanryan52@gmail.com>  Wed, 08 Apr 2009 19:09:46 -0700

libcatalyst-modules-perl (32) unstable; urgency=low

  [ Krzysztof Krzyżaniak (eloy) ]
  * Removed modules:
   + Test::WWW::Mechanize::Catalyst -- has now separate package
  * Updated modules:
   + Catalyst::Model::DBI 0.20
   + Catalyst::Model::DBIC::Schema 0.23
   + Catalyst::Plugin::Cache 0.08
   + Catalyst::Plugin::ConfigLoader 0.22
   + Catalyst::Plugin::Session 0.20
   + Catalyst::Plugin::StackTrace 0.09
   + Catalyst::Devel 1.10
   + Catalyst::Plugin::Session::State::Cookie 0.10
   + Catalyst::Plugin::Session::Store::DBIC 0.08
   + Catalyst::Plugin::Session::Store::Delegate 0.03
   + Catalyst::Plugin::Session::Store::FastMmap 0.06
   + Catalyst::Plugin::Authentication 0.10011
   + Catalyst::Authentication::Credential::Authen::Simple 0.02
   + Catalyst::Authentication::Store::DBIx::Class 0.1082
   + Catalyst::Plugin::Authorization::ACL 0.10
   + Catalyst::Plugin::Authorization::Roles 0.07
   + Catalyst::Plugin::I18N 0.08
   + Catalyst::View::Email 0.13
   + Catalyst::Action::RenderView 0.09
   + Catalyst::Log::Log4perl 1.03
  * debian/control: Standards-Version updated to 3.8.1

  [ Martín Ferrari ]
  * debian/control: Change Maintainer to Perl group.

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Thu, 05 Feb 2009 10:57:16 +0100

libcatalyst-modules-perl (31) unstable; urgency=low

  * Removed (created as separate package libcatalyst-manual-perl):
   + Catalyst::Manual (closes: #487506)
  * Updated modules:
   + Catalyst::Devel 1.08
   + Catalyst::View-Email 0.11
  * debian/control:
   + Depends: moved libjson-any-perl here from Suggests (closes: #489534)

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Fri, 11 Jul 2008 17:15:45 +0200

libcatalyst-modules-perl (30) unstable; urgency=low

  * debian/control:
   + Standards-Version: updated 3.8.0 without additional changes
   + Build-Depends: debhelper increased version to (>= 7)
  * debian/compat: increased to 7 without additional changes
  * Added modules:
   + Catalyst::Authentication::Credential::Authen::Simple
  * Removed modules:
   + Catalyst::Plugin::Authentication::Store::DBIC
  * Updated modules:
   + Catalyst::Manual 5.7012
   + Catalyst::Plugin::Cache 0.06
   + Catalyst::Devel 1.07
   + Catalyst::View::Mason 0.16

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Mon, 30 Jun 2008 14:13:29 +0200

libcatalyst-modules-perl (29) unstable; urgency=low

  * debian/control: narrow dependency from libcatalyst-perl to (>= 5.7012-7)
  * Updated modules:
   + Test::WWW::Mechanize::Catalyst 0.42
   + Catalyst::Action::RenderView 0.08
   + Catalyst::Plugin::ConfigLoader 0.20
   + Catalyst::Plugin::Session::Store::DBI 0.13

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Thu, 24 Apr 2008 12:39:41 +0200

libcatalyst-modules-perl (28) unstable; urgency=low

  * Bugfix release (closes: #476589)
  * Catalyst::Plugin::Authentication::Store::DBIx::Class changed name to
   Catalyst::Authentication::Store::DBIx::Class
  * Added Catalyst::View::Email (closes: #472879)
  * debian/control:
   + updated dependencies

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Thu, 24 Apr 2008 11:58:41 +0200

libcatalyst-modules-perl (27) unstable; urgency=low

  * Remove debian/preinst debian/postrm
  * Updated modules:
   + Catalyst::Plugin::Dumper 0.00005

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Sun, 20 Apr 2008 18:03:32 +0200

libcatalyst-modules-perl (26) unstable; urgency=low

  * Updated modules:
   + Catalyst::Manual 5.701004
   + Catalyst::Plugin::StackTrace 0.08
   + Catalyst::Devel 1.06
   + Catalyst::View::JSON 0.24
   + Catalyst::Plugin::Authentication 0.10006
   + Catalyst::Plugin::Dumper 0.00004

 -- Krzysztof Krzyżaniak (eloy) <eloy@debian.org>  Wed, 16 Apr 2008 14:09:09 +0200

libcatalyst-modules-perl (25) unstable; urgency=low

  * Updated modules:
   + Catalyst::Plugin::Cache 0.05
   + Catalyst::Plugin::Session::Store::FastMmap 0.05
   + Catalyst::Plugin::Session::Store::File 0.13
   + Catalyst::Plugin::Authentication 0.10005

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sun, 03 Feb 2008 14:45:20 +0100

libcatalyst-modules-perl (24) unstable; urgency=low

  * Updated modules:
   + Catalyst::Plugin::Session::Store::FastMmap 0.04
   + Catalyst::View::JSON 0.22
   + Catalyst::Plugin::Session::Store::File 0.12

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 16 Jan 2008 17:07:37 +0100

libcatalyst-modules-perl (23) unstable; urgency=low

  * Updated modules:
   + Catalyst::Manual 5.701003
   + Catalyst::Plugin::ConfigLoader 0.19
   + Catalyst::Plugin::Session 0.19
   + Catalyst::Plugin::Session::State::Cookie 0.09
   + Catalyst::Plugin::Session::Store::DBI 0.12
   + Catalyst::View::JSON 0.21
   + Catalyst::View::Mason 0.15
   + Catalyst::Plugin::Authentication 0.10004
   + Catalyst::Plugin::SubRequest 0.13
   + Catalyst::Plugin::I18N 0.07
  * debian/control:
   + changed dependency for  libcatalyst-perl (>= 5.7008) (closes: #457282)
   + Standards-Version increased to 3.7.3 without changes

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 22 Oct 2007 11:00:17 +0200

libcatalyst-modules-perl (22) unstable; urgency=low

  * Updated modules:
   + Test-WWW-Mechanize-Catalyst 0.41
   + Catalyst-Action-RenderView 0.07
   + Catalyst-Plugin-Static-Simple 0.20
   + Catalyst-Plugin-SubRequest 0.12
   + Catalyst-Plugin-Unicode 0.8
   + Catalyst-Plugin-Session-State-Cookie 0.08
   + Catalyst-Plugin-Authentication-Store-DBIC 0.09

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 04 Oct 2007 12:31:02 +0200

libcatalyst-modules-perl (21) unstable; urgency=low

  * Updated plugins:
   + Test::WWW::Mechanize::Catalyst 0.40
   + Catalyst::Action::RenderView 0.06
   + Catalyst::Manual 5.701002
   + Catalyst::Plugin::ConfigLoader 0.17
   + Catalyst::Plugin::Session 0.18
   + Catalyst::Plugin::Unicode 0.5
   + Catalyst::Devel 1.03
   + Catalyst::View::JSON 0.20
   + Catalyst::View::Mason 0.13
   + Catalyst::Plugin::Authentication 0.10002
   + Catalyst::Plugin::I18N 0.06

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 13 Jul 2007 12:21:56 +0200

libcatalyst-modules-perl (20) unstable; urgency=low

  * New plugins:
   + Catalyst::Plugin::Authentication::Store::DBIx::Class 0.10
  * Updated Plugins
   + Test::WWW::Mechanize::Catalyst 0.38
   + Catalyst::Action::RenderView 0.05
   + Catalyst::Log::Log4perl 1.00
   + Catalyst::Plugin::Cache 0.03
   + Catalyst::Plugin::Session 0.16
   + Catalyst::Plugin::Static::Simple 0.19
   + Catalyst::Plugin::Unicode 0.3
   + Catalyst::Plugin::Cache::Store::FastMmap 0.02
   + Catalyst::Plugin::Session::Store::FastMmap 0.03
   + Catalyst::Plugin::Authentication 0.10000
   + Catalyst::Controller::FormBuilder 0.04

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 24 May 2007 10:43:59 +0200

libcatalyst-modules-perl (19) unstable; urgency=low

  * Catalyst::Model::DBIC::Schema updated to 0.20

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 24 May 2007 10:35:41 +0200

libcatalyst-modules-perl (18) unstable; urgency=low

  * debian/control: libsub-install-perl added to dependencies (closes: #423734)
  * Catalyst::Plugin::Static::Simple updated to 0.17
  * Catalyst::Model::DBI updated to 0.19.tar.gz

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 14 May 2007 15:09:29 +0200

libcatalyst-modules-perl (17) unstable; urgency=low

  * Added Catalyst::View::Mason 0.08
  * Catalyst::Controller::FormBuilder updated to 0.03
  * Catalyst::View::JSON updated to 0.18
  * Catalyst::Plugin::Session::State::Cookie updated to 0.07
  * Catalyst::Plugin::SubRequest updated to 0.11
  * Catalyst::Plugin::ConfigLoader updated to 0.14
  * Catalyst::Manual updated to 5.700701
  * Catalyst::Log::Log4perl updated to 0.51
  * Catalyst::Plugin::Session updated to 0.14
  * Catalyst::Plugin::Session::Store::DBI updated to 0.11
  * Catalyst::Plugin::Prototype updated to 1.33
  * Catalyst::Plugin::Static::Simple updated to 0.16
  * Catalyst::Model::DBI updated to 0.18

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 21 Feb 2007 14:04:44 +0100

libcatalyst-modules-perl (16) unstable; urgency=low

  * Added Catalyst::Controller::FormBuilder
  * Added Catalyst::Plugin::Flavour
  * Catalyst::View::JSON updated to 0.14
  * Catalyst::Plugin::Server updated to 0.24

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 28 Dec 2006 14:26:13 +0100

libcatalyst-modules-perl (15) unstable; urgency=low

  * Catalyst::Devel updated to 1.02
  * Catalyst::Plugin::Server updated to 0.22
  * Catalyst::Model::DBI updated to 0.15
  * Catalyst::Plugin::Static::Simple updated to 0.15

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sun, 26 Nov 2006 15:59:04 +0100

libcatalyst-modules-perl (14) unstable; urgency=low

  * Added Class::Model::DBI

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sun, 26 Nov 2006 15:47:28 +0100

libcatalyst-modules-perl (13) unstable; urgency=low

  * Catalyst::Manual updated to  5.700501 (closes: #397989)

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 13 Nov 2006 11:25:42 +0100

libcatalyst-modules-perl (12) unstable; urgency=low

  * Added Catalyst::Manual
  * Catalyst::Plugin::Session::Store::File updated to 0.10
  * Catalyst::View::JSON updated to 0.12

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 26 Oct 2006 15:38:33 +0200

libcatalyst-modules-perl (11) unstable; urgency=low

  * Catalyst::Plugin::Session updated to 0.13
  * Catalyst::Plugin::Authorization::Roles updated to 0.05

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 16 Oct 2006 13:25:51 +0200

libcatalyst-modules-perl (10) unstable; urgency=low

  * Added Catalyst::View::JSON 0.11
  * Added Catalyst::Plugin::Unicode 0.2
  * Catalyst::Plugin::Session::State::Cookie updated to 0.06

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 26 Sep 2006 23:08:47 +0200

libcatalyst-modules-perl (9) unstable; urgency=low

  * Added Catalyst::Plugin::Session::Store::Delegate 0.02
  * Catalyst::Plugin::Cache updated to 0.02
  * Catalyst::Devel updated to 1.01
  * Catalyst::Plugin::Session::Store::DBIC updated to 0.06

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 19 Sep 2006 13:11:39 +0200

libcatalyst-modules-perl (8) unstable; urgency=low

  * Added Test::WWW::Mechanize::Catalyst
  * Added Catalyst::Plugin::Server which obsoletes Catalyst::Plugin::XMLRPC
    (Catalyst::Plugin::XMLRPC will be removed in next version of package)
  * Catalyst::Plugin::Session upgraded to 0.12
  * Catalyst::Plugin::ConfigLoader updated to 0.13

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 11 Aug 2006 12:19:18 +0200

libcatalyst-modules-perl (7) unstable; urgency=low

  * Added Catalyst::Plugin::Cache::Store::FastMmap
  * Catalyst::Model::DBIC::Schema upgraded to 0.18
  * Catalyst::Plugin::Session::State::Cookie upgraded to 0.05

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue,  1 Aug 2006 20:14:17 +0200

libcatalyst-modules-perl (6) unstable; urgency=low

  * debian/control:
   + libdata-visitor-perl added to dependencies
  * Catalyst::Plugin::Session upgraded to 0.10
  * Catalyst::Plugin::ConfigLoader upgraded to 0.12
  * Catalyst::Plugin::Session::Store::DBIC upgraded to 0.05
  * Catalyst::Plugin::Session::State::Cookie upgraded to 0.04
  * Catalyst::Plugin::Authentication upgraded to 0.09

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Sat, 29 Jul 2006 20:58:59 +0200

libcatalyst-modules-perl (5) unstable; urgency=low

  * Added Catalyst::Plugin::Cache
  * Catalyst::Plugin::Session upgraded to 0.06
  * Catalyst::Plugin::Session::State::Cookie upgraded to 0.03
  * Catalyst::Plugin::Authentication upgraded to 0.08

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 27 Jul 2006 17:30:27 +0200

libcatalyst-modules-perl (4) unstable; urgency=low

  * Catalyst::Plugin::Session::Store::File upgraded to 0.08
  * Catalyst::Plugin::Authorization::ACL upgraded to 0.08
  * Catalyst::Plugin::StackTrace upgraded to 0.06

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Tue, 25 Jul 2006 14:34:19 +0200

libcatalyst-modules-perl (3) unstable; urgency=low

  * Catalyst::Plugin::StackTrace upgraded to 0.05
  * debian/control:
   + Depends: added missing libhtml-prototype-perl

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 14 Jul 2006 12:38:13 +0200

libcatalyst-modules-perl (2) unstable; urgency=low

  * Catalyst::Plugin::ConfigLoader upgraded to 0.11

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu, 13 Jul 2006 15:13:48 +0200

libcatalyst-modules-perl (1) unstable; urgency=low

  [ Krzysztof Krzyzaniak (eloy) ]
  * configuration of packages moved from YAML files to Config::General file.
  * debian/control:
   + dependency revisited
  * Added Catalyst::Action::RenderView
  * Added Catalyst::Plugin::XMLRPC
  * Added Catalyst::Plugin::Session::Store::DBIC
  * Added Catalyst::Plugin::Dumper
  * Added Catalyst::Plugin::I18N
  * Added Catalyst::Plugin::ConfigLoader
  * Added Catalyst::Plugin::Static::Simple
  * Added Catalyst::Devel
  * Catalyst::Log::Log4perl updated to 0.4
  * Catalyst::Plugin::Authentication::Store::DBIC updated to 0.07
  * Catalyst::Plugin::Authorization::ACL updated to 0.07
  * Catalyst::Plugin::FormValidator::Simple moved to
    libcatalyst-modules-extra-perl package
  * Catalyst::Plugin::DefaultEnd updated to 0.06
  * Catalyst::Model::DBIC::Schema updated to 0.16.

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Wed, 26 Apr 2006 11:03:48 +0200

libcatalyst-modules-perl (0.13) unstable; urgency=low

  * Added Catalyst::Log::Log4perl 0.3
  * Removed obsoleted Catalyst::Plugin::Authentication::CDBI
  * Catalyst::Helper::Controller::Scaffold and Catalyst::Model::CDBI::CRUD moved
    to libcatalyst-modules-extra-perl package

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 14 Apr 2006 23:35:12 +0200

libcatalyst-modules-perl (0.12) unstable; urgency=low

  [ Krzysztof Krzyzaniak (eloy) ]
  * Catalyst::Model::DBIC::Schema updated to 0.11
  * Catalyst::Plugin::StackTrace updated to 0.04
  * Catalyst::Plugin::Authorization::Roles updated to 0.04

  [ Florian Ragwitz ]
  * Changed my email address in the Uploader field.
  * Added Catalyst::Plugin::DefaultEnd 0.05.
  * Better layout for tarballs/check-upstream-versions output.
  * Updated Catalyst::Plugin::Session::Store::DBI to 0.07.

 -- Florian Ragwitz <rafl@debian.org>  Fri,  7 Apr 2006 20:35:02 +0200

libcatalyst-modules-perl (0.11) unstable; urgency=low

  * Added Catalyst::Model::DBIC::Schema 0.08, updated dependencies
  * Catalyst::Plugin::Authentication::Store::DBIC updated to 0.05002
  * Catalyst::Plugin::StackTrace updated to 0.03
  * Catalyst::Plugin::Authentication updated to 0.07

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu,  9 Mar 2006 16:25:28 +0100

libcatalyst-modules-perl (0.10) unstable; urgency=low

  * Catalyst::Plugin::FormValidator::Simple updated to 0.09
  * Catalyst::Plugin::Authentication::Store::DBIC updated to 0.05001

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri,  3 Mar 2006 22:30:55 +0100

libcatalyst-modules-perl (0.09) unstable; urgency=low

  * changes in debian/make-module.sh
  * rewritten debian/rules
  * changed layout of tarballs directory
  * Catalyst::Plugin::HTML::Widget added

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 20 Feb 2006 23:08:50 +0100

libcatalyst-modules-perl (0.08) unstable; urgency=low

  * new debian/make-module.sh script
  * Added Catalyst::Plugin::Session suite
  * Added Catalyst::Plugin::Authentication suite
  * Added Catalyst::Plugin::Authorization suite
  * Added script for checking upstream versions
  * debian/control - updated dependencies, autogenreated description
  * debian/copyright - autogenerated
  * Mark Catalyst::Plugin::Authentication::CDBI as obsolete

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 27 Jan 2006 10:07:11 +0100

libcatalyst-modules-perl (0.07) unstable; urgency=low

  * Added Catalyst::Plugin::StackTrace

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Thu,  5 Jan 2006 12:46:01 +0100

libcatalyst-modules-perl (0.06) unstable; urgency=low

  * Added Catalyst::Plugin::FormValidator::Simple
  * Catalyst::Helper::Controller::Scaffold updated to 0.04
  * Catalyst::Plugin::Authentication::CDBI updated to 0.10

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Fri, 23 Dec 2005 23:37:05 +0100

libcatalyst-modules-perl (0.05) unstable; urgency=low

  * Added Catalyst::Plugin::SubRequest

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 12 Dec 2005 19:56:02 +0100

libcatalyst-modules-perl (0.04) unstable; urgency=low

  * Added Catalyst::Plugin::Prototype
  * Catalyst::Model::CDBI::CRUD updated to 0.04

 -- Krzysztof Krzyzaniak (eloy) <eloy@debian.org>  Mon, 21 Nov 2005 14:45:06 +0100

libcatalyst-modules-perl (0.03) unstable; urgency=low

  * Fixed debian/control. It was totally broken. Dependencies to
    build-essential and cdbs were removed, the right dependencies were added.
  * Added C::H::Controller::Scaffold.

 -- Florian Ragwitz <rafl@debianforum.de>  Thu, 22 Sep 2005 01:11:17 +0200

libcatalyst-modules-perl (0.02) unstable; urgency=low

  * Updated C::M::CDBI::CRUD to 0.03.

 -- Florian Ragwitz <rafl@debianforum.de>  Wed, 10 Aug 2005 16:59:57 +0200

libcatalyst-modules-perl (0.01) unstable; urgency=low

  * Initial release.

 -- Florian Ragwitz <rafl@debianforum.de>  Wed, 10 Aug 2005 10:44:57 +0200
